package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        try {

            if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();
            Collections.sort(inputNumbers);

            Double numberInTriangleSequence = (Math.sqrt(8 * inputNumbers.size() + 1) - 1) / 2;


            int[][] requiredPyramid;

            if (numberInTriangleSequence % 1 == 0) {
//                find width\height using number in triangle sequence
                int pyramidWidth = (numberInTriangleSequence.intValue() * 2) - 1;
                int pyramidHeight = numberInTriangleSequence.intValue();


                int[][] tempPyramid = new int[pyramidHeight][pyramidHeight];
                int counter = 0;
                for (int i = 0; i < pyramidHeight; i++) {
                    for (int j = 0; j <= i; j++) {
                        tempPyramid[i][j] = inputNumbers.get(counter);
                        counter++;
                    }
                }


                requiredPyramid = new int[pyramidHeight][pyramidWidth];

                int startPoint = (int) Math.ceil(pyramidWidth / 2) - 1;

                for (int i = 1; i <= pyramidHeight; i++) {
                    int gap = 1;
                    for (int j = 1; j <= i; j++) {
                        requiredPyramid[i - 1][startPoint + gap] = tempPyramid[i - 1][j - 1];
                        gap+= 2;
                    }
                    startPoint--;
                }


            } else {throw new CannotBuildPyramidException();}

            return requiredPyramid;

        } catch (Throwable t){throw new CannotBuildPyramidException();}

    }

}
