package com.tsystems.javaschool.tasks.zones;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RouteChecker {


    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     *
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     *  - A has link to B
     *  - OR B has a link to A
     *  - OR both of them have a link to each other
     *
     * @param zoneState current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */
    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds){
        // TODO : Implement your solution here

        List<Zone> requestedZones = new ArrayList();
        Set<Integer> adjacentZones = new HashSet<>();


        for (Integer i : requestedZoneIds){
            for (Zone z : zoneState){
                if (z.getId()==i) requestedZones.add(z);
            }
        }


        for (int i = 0; i <requestedZones.size() ; i++) {
            Zone zone = requestedZones.get(i);
            for (int j = i+1; j <requestedZones.size() ; j++) {
                Zone zoneNext = requestedZones.get(j);
                if (zone.getNeighbours().contains(zoneNext.getId()) || zoneNext.getNeighbours().contains(zone.getId())){
                    adjacentZones.add(zone.getId());
                    adjacentZones.add(zoneNext.getId());
                }
            }
        }


        if (adjacentZones.size()!= requestedZoneIds.size()) return false;

        return true;
    }

}
